package co.edu.usbcali.bodyfitness.modelo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "ejercicio", schema = "public")
public class Ejercicio implements java.io.Serializable {
    @NotNull
    private Long ejeId;
    @NotNull
    private TipoEjercicio tipoEjercicio;
    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String activo;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String descripcion;
    @NotNull
    private Date fechaCreacion;
    private Date fechaModificacion;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String imagen;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String nombre;
    @NotNull
    private Integer repeticiones;
    @NotNull
    private Integer series;
    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<EjercicioRutina> ejercicioRutinas = new HashSet<EjercicioRutina>(0);

    public Ejercicio() {
    }

    public Ejercicio(Long ejeId, String activo, String descripcion,
        Set<EjercicioRutina> ejercicioRutinas, Date fechaCreacion,
        Date fechaModificacion, String imagen, String nombre,
        Integer repeticiones, Integer series, TipoEjercicio tipoEjercicio,
        String usuarioCreador, String usuarioModificador) {
        this.ejeId = ejeId;
        this.tipoEjercicio = tipoEjercicio;
        this.activo = activo;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.imagen = imagen;
        this.nombre = nombre;
        this.repeticiones = repeticiones;
        this.series = series;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.ejercicioRutinas = ejercicioRutinas;
    }

    @Id
    @Column(name = "eje_id", unique = true, nullable = false)
    public Long getEjeId() {
        return this.ejeId;
    }

    public void setEjeId(Long ejeId) {
        this.ejeId = ejeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tieje_id")
    public TipoEjercicio getTipoEjercicio() {
        return this.tipoEjercicio;
    }

    public void setTipoEjercicio(TipoEjercicio tipoEjercicio) {
        this.tipoEjercicio = tipoEjercicio;
    }

    @Column(name = "activo", nullable = false)
    public String getActivo() {
        return this.activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    @Column(name = "descripcion", nullable = false)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "imagen", nullable = false)
    public String getImagen() {
        return this.imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "repeticiones", nullable = false)
    public Integer getRepeticiones() {
        return this.repeticiones;
    }

    public void setRepeticiones(Integer repeticiones) {
        this.repeticiones = repeticiones;
    }

    @Column(name = "series", nullable = false)
    public Integer getSeries() {
        return this.series;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ejercicio")
    public Set<EjercicioRutina> getEjercicioRutinas() {
        return this.ejercicioRutinas;
    }

    public void setEjercicioRutinas(Set<EjercicioRutina> ejercicioRutinas) {
        this.ejercicioRutinas = ejercicioRutinas;
    }
}
