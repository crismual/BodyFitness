package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.Antropometria;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface AntropometriaMapper {
    public AntropometriaDTO antropometriaToAntropometriaDTO(
        Antropometria antropometria) throws Exception;

    public Antropometria antropometriaDTOToAntropometria(
        AntropometriaDTO antropometriaDTO) throws Exception;

    public List<AntropometriaDTO> listAntropometriaToListAntropometriaDTO(
        List<Antropometria> antropometrias) throws Exception;

    public List<Antropometria> listAntropometriaDTOToListAntropometria(
        List<AntropometriaDTO> antropometriaDTOs) throws Exception;
}
