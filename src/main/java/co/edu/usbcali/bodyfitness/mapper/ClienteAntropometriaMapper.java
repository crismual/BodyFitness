package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ClienteAntropometriaMapper {
    public ClienteAntropometriaDTO clienteAntropometriaToClienteAntropometriaDTO(
        ClienteAntropometria clienteAntropometria) throws Exception;

    public ClienteAntropometria clienteAntropometriaDTOToClienteAntropometria(
        ClienteAntropometriaDTO clienteAntropometriaDTO)
        throws Exception;

    public List<ClienteAntropometriaDTO> listClienteAntropometriaToListClienteAntropometriaDTO(
        List<ClienteAntropometria> clienteAntropometrias)
        throws Exception;

    public List<ClienteAntropometria> listClienteAntropometriaDTOToListClienteAntropometria(
        List<ClienteAntropometriaDTO> clienteAntropometriaDTOs)
        throws Exception;
}
