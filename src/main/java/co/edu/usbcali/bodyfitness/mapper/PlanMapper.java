package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.PlanDTO;
import co.edu.usbcali.bodyfitness.modelo.Plan;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface PlanMapper {
    public PlanDTO planToPlanDTO(Plan plan) throws Exception;

    public Plan planDTOToPlan(PlanDTO planDTO) throws Exception;

    public List<PlanDTO> listPlanToListPlanDTO(List<Plan> plans)
        throws Exception;

    public List<Plan> listPlanDTOToListPlan(List<PlanDTO> planDTOs)
        throws Exception;
}
