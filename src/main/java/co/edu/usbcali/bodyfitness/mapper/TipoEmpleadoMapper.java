package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.TipoEmpleadoDTO;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface TipoEmpleadoMapper {
    public TipoEmpleadoDTO tipoEmpleadoToTipoEmpleadoDTO(
        TipoEmpleado tipoEmpleado) throws Exception;

    public TipoEmpleado tipoEmpleadoDTOToTipoEmpleado(
        TipoEmpleadoDTO tipoEmpleadoDTO) throws Exception;

    public List<TipoEmpleadoDTO> listTipoEmpleadoToListTipoEmpleadoDTO(
        List<TipoEmpleado> tipoEmpleados) throws Exception;

    public List<TipoEmpleado> listTipoEmpleadoDTOToListTipoEmpleado(
        List<TipoEmpleadoDTO> tipoEmpleadoDTOs) throws Exception;
}
