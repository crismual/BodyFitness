package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.TipoEjercicioDTO;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface TipoEjercicioMapper {
    public TipoEjercicioDTO tipoEjercicioToTipoEjercicioDTO(
        TipoEjercicio tipoEjercicio) throws Exception;

    public TipoEjercicio tipoEjercicioDTOToTipoEjercicio(
        TipoEjercicioDTO tipoEjercicioDTO) throws Exception;

    public List<TipoEjercicioDTO> listTipoEjercicioToListTipoEjercicioDTO(
        List<TipoEjercicio> tipoEjercicios) throws Exception;

    public List<TipoEjercicio> listTipoEjercicioDTOToListTipoEjercicio(
        List<TipoEjercicioDTO> tipoEjercicioDTOs) throws Exception;
}
