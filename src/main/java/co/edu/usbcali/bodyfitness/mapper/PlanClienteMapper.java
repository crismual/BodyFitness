package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface PlanClienteMapper {
    public PlanClienteDTO planClienteToPlanClienteDTO(PlanCliente planCliente)
        throws Exception;

    public PlanCliente planClienteDTOToPlanCliente(
        PlanClienteDTO planClienteDTO) throws Exception;

    public List<PlanClienteDTO> listPlanClienteToListPlanClienteDTO(
        List<PlanCliente> planClientes) throws Exception;

    public List<PlanCliente> listPlanClienteDTOToListPlanCliente(
        List<PlanClienteDTO> planClienteDTOs) throws Exception;
}
