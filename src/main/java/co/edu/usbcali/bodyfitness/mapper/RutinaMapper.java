package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.Rutina;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface RutinaMapper {
    public RutinaDTO rutinaToRutinaDTO(Rutina rutina) throws Exception;

    public Rutina rutinaDTOToRutina(RutinaDTO rutinaDTO)
        throws Exception;

    public List<RutinaDTO> listRutinaToListRutinaDTO(List<Rutina> rutinas)
        throws Exception;

    public List<Rutina> listRutinaDTOToListRutina(List<RutinaDTO> rutinaDTOs)
        throws Exception;
}
