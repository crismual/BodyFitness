package co.edu.usbcali.bodyfitness.mapper;

import co.edu.usbcali.bodyfitness.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.modelo.Pago;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface PagoMapper {
    public PagoDTO pagoToPagoDTO(Pago pago) throws Exception;

    public Pago pagoDTOToPago(PagoDTO pagoDTO) throws Exception;

    public List<PagoDTO> listPagoToListPagoDTO(List<Pago> pagos)
        throws Exception;

    public List<Pago> listPagoDTOToListPago(List<PagoDTO> pagoDTOs)
        throws Exception;
}
