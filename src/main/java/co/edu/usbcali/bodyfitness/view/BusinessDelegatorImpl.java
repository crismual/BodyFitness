package co.edu.usbcali.bodyfitness.view;

import co.edu.usbcali.bodyfitness.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.dto.EmpleadoDTO;
import co.edu.usbcali.bodyfitness.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.dto.PlanDTO;
import co.edu.usbcali.bodyfitness.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.dto.TipoEjercicioDTO;
import co.edu.usbcali.bodyfitness.dto.TipoEmpleadoDTO;
import co.edu.usbcali.bodyfitness.dto.TipoPagoDTO;
import co.edu.usbcali.bodyfitness.modelo.Antropometria;
import co.edu.usbcali.bodyfitness.modelo.Cliente;
import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;
import co.edu.usbcali.bodyfitness.modelo.Ejercicio;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;
import co.edu.usbcali.bodyfitness.modelo.Empleado;
import co.edu.usbcali.bodyfitness.modelo.Pago;
import co.edu.usbcali.bodyfitness.modelo.Plan;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;
import co.edu.usbcali.bodyfitness.modelo.Rutina;
import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;
import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;
import co.edu.usbcali.bodyfitness.modelo.TipoPago;
import co.edu.usbcali.bodyfitness.service.AntropometriaService;
import co.edu.usbcali.bodyfitness.service.ClienteAntropometriaService;
import co.edu.usbcali.bodyfitness.service.ClienteRutinaService;
import co.edu.usbcali.bodyfitness.service.ClienteService;
import co.edu.usbcali.bodyfitness.service.EjercicioRutinaService;
import co.edu.usbcali.bodyfitness.service.EjercicioService;
import co.edu.usbcali.bodyfitness.service.EmpleadoService;
import co.edu.usbcali.bodyfitness.service.PagoService;
import co.edu.usbcali.bodyfitness.service.PlanClienteService;
import co.edu.usbcali.bodyfitness.service.PlanService;
import co.edu.usbcali.bodyfitness.service.RutinaService;
import co.edu.usbcali.bodyfitness.service.TipoEjercicioService;
import co.edu.usbcali.bodyfitness.service.TipoEmpleadoService;
import co.edu.usbcali.bodyfitness.service.TipoPagoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import java.sql.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Use a Business Delegate to reduce coupling between presentation-tier clients and business services.
* The Business Delegate hides the underlying implementation details of the business service, such as lookup and access details of the EJB architecture.
*
* The Business Delegate acts as a client-side business abstraction; it provides an abstraction for, and thus hides,
* the implementation of the business services. Using a Business Delegate reduces the coupling between presentation-tier clients and
* the system's business services. Depending on the implementation strategy, the Business Delegate may shield clients from possible
* volatility in the implementation of the business service API. Potentially, this reduces the number of changes that must be made to the
* presentation-tier client code when the business service API or its underlying implementation changes.
*
* However, interface methods in the Business Delegate may still require modification if the underlying business service API changes.
* Admittedly, though, it is more likely that changes will be made to the business service rather than to the Business Delegate.
*
* Often, developers are skeptical when a design goal such as abstracting the business layer causes additional upfront work in return
* for future gains. However, using this pattern or its strategies results in only a small amount of additional upfront work and provides
* considerable benefits. The main benefit is hiding the details of the underlying service. For example, the client can become transparent
* to naming and lookup services. The Business Delegate also handles the exceptions from the business services, such as java.rmi.Remote
* exceptions, Java Messages Service (JMS) exceptions and so on. The Business Delegate may intercept such service level exceptions and
* generate application level exceptions instead. Application level exceptions are easier to handle by the clients, and may be user friendly.
* The Business Delegate may also transparently perform any retry or recovery operations necessary in the event of a service failure without
* exposing the client to the problem until it is determined that the problem is not resolvable. These gains present a compelling reason to
* use the pattern.
*
* Another benefit is that the delegate may cache results and references to remote business services. Caching can significantly improve performance,
* because it limits unnecessary and potentially costly round trips over the network.
*
* A Business Delegate uses a component called the Lookup Service. The Lookup Service is responsible for hiding the underlying implementation
* details of the business service lookup code. The Lookup Service may be written as part of the Delegate, but we recommend that it be
* implemented as a separate component, as outlined in the Service Locator pattern (See "Service Locator" on page 368.)
*
* When the Business Delegate is used with a Session Facade, typically there is a one-to-one relationship between the two.
* This one-to-one relationship exists because logic that might have been encapsulated in a Business Delegate relating to its interaction
* with multiple business services (creating a one-to-many relationship) will often be factored back into a Session Facade.
*
* Finally, it should be noted that this pattern could be used to reduce coupling between other tiers, not simply the presentation and the
* business tiers.
*
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Scope("singleton")
@Component("businessDelegator")
public class BusinessDelegatorImpl implements BusinessDelegator {
    private static final Logger log = LoggerFactory.getLogger(BusinessDelegatorImpl.class);
    @Autowired
    private AntropometriaService antropometriaService;
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteAntropometriaService clienteAntropometriaService;
    @Autowired
    private ClienteRutinaService clienteRutinaService;
    @Autowired
    private EjercicioService ejercicioService;
    @Autowired
    private EjercicioRutinaService ejercicioRutinaService;
    @Autowired
    private EmpleadoService empleadoService;
    @Autowired
    private PagoService pagoService;
    @Autowired
    private PlanService planService;
    @Autowired
    private PlanClienteService planClienteService;
    @Autowired
    private RutinaService rutinaService;
    @Autowired
    private TipoEjercicioService tipoEjercicioService;
    @Autowired
    private TipoEmpleadoService tipoEmpleadoService;
    @Autowired
    private TipoPagoService tipoPagoService;

    public List<Antropometria> getAntropometria() throws Exception {
        return antropometriaService.getAntropometria();
    }

    public void saveAntropometria(Antropometria entity)
        throws Exception {
        antropometriaService.saveAntropometria(entity);
    }

    public void deleteAntropometria(Antropometria entity)
        throws Exception {
        antropometriaService.deleteAntropometria(entity);
    }

    public void updateAntropometria(Antropometria entity)
        throws Exception {
        antropometriaService.updateAntropometria(entity);
    }

    public Antropometria getAntropometria(Long antId) throws Exception {
        Antropometria antropometria = null;

        try {
            antropometria = antropometriaService.getAntropometria(antId);
        } catch (Exception e) {
            throw e;
        }

        return antropometria;
    }

    public List<Antropometria> findByCriteriaInAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return antropometriaService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Antropometria> findPageAntropometria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return antropometriaService.findPageAntropometria(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberAntropometria() throws Exception {
        return antropometriaService.findTotalNumberAntropometria();
    }

    public List<AntropometriaDTO> getDataAntropometria()
        throws Exception {
        return antropometriaService.getDataAntropometria();
    }

    public void validateAntropometria(Antropometria antropometria)
        throws Exception {
        antropometriaService.validateAntropometria(antropometria);
    }

    public List<Cliente> getCliente() throws Exception {
        return clienteService.getCliente();
    }

    public void saveCliente(Cliente entity) throws Exception {
        clienteService.saveCliente(entity);
    }

    public void deleteCliente(Cliente entity) throws Exception {
        clienteService.deleteCliente(entity);
    }

    public void updateCliente(Cliente entity) throws Exception {
        clienteService.updateCliente(entity);
    }

    public Cliente getCliente(Long cliId) throws Exception {
        Cliente cliente = null;

        try {
            cliente = clienteService.getCliente(cliId);
        } catch (Exception e) {
            throw e;
        }

        return cliente;
    }

    public List<Cliente> findByCriteriaInCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return clienteService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Cliente> findPageCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return clienteService.findPageCliente(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberCliente() throws Exception {
        return clienteService.findTotalNumberCliente();
    }

    public List<ClienteDTO> getDataCliente() throws Exception {
        return clienteService.getDataCliente();
    }

    public void validateCliente(Cliente cliente) throws Exception {
        clienteService.validateCliente(cliente);
    }

    public List<ClienteAntropometria> getClienteAntropometria()
        throws Exception {
        return clienteAntropometriaService.getClienteAntropometria();
    }

    public void saveClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaService.saveClienteAntropometria(entity);
    }

    public void deleteClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaService.deleteClienteAntropometria(entity);
    }

    public void updateClienteAntropometria(ClienteAntropometria entity)
        throws Exception {
        clienteAntropometriaService.updateClienteAntropometria(entity);
    }

    public ClienteAntropometria getClienteAntropometria(Long cliantId)
        throws Exception {
        ClienteAntropometria clienteAntropometria = null;

        try {
            clienteAntropometria = clienteAntropometriaService.getClienteAntropometria(cliantId);
        } catch (Exception e) {
            throw e;
        }

        return clienteAntropometria;
    }

    public List<ClienteAntropometria> findByCriteriaInClienteAntropometria(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return clienteAntropometriaService.findByCriteria(variables,
            variablesBetween, variablesBetweenDates);
    }

    public List<ClienteAntropometria> findPageClienteAntropometria(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return clienteAntropometriaService.findPageClienteAntropometria(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberClienteAntropometria() throws Exception {
        return clienteAntropometriaService.findTotalNumberClienteAntropometria();
    }

    public List<ClienteAntropometriaDTO> getDataClienteAntropometria()
        throws Exception {
        return clienteAntropometriaService.getDataClienteAntropometria();
    }

    public void validateClienteAntropometria(
        ClienteAntropometria clienteAntropometria) throws Exception {
        clienteAntropometriaService.validateClienteAntropometria(clienteAntropometria);
    }

    public List<ClienteRutina> getClienteRutina() throws Exception {
        return clienteRutinaService.getClienteRutina();
    }

    public void saveClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaService.saveClienteRutina(entity);
    }

    public void deleteClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaService.deleteClienteRutina(entity);
    }

    public void updateClienteRutina(ClienteRutina entity)
        throws Exception {
        clienteRutinaService.updateClienteRutina(entity);
    }

    public ClienteRutina getClienteRutina(Long clirutId)
        throws Exception {
        ClienteRutina clienteRutina = null;

        try {
            clienteRutina = clienteRutinaService.getClienteRutina(clirutId);
        } catch (Exception e) {
            throw e;
        }

        return clienteRutina;
    }

    public List<ClienteRutina> findByCriteriaInClienteRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return clienteRutinaService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<ClienteRutina> findPageClienteRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return clienteRutinaService.findPageClienteRutina(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberClienteRutina() throws Exception {
        return clienteRutinaService.findTotalNumberClienteRutina();
    }

    public List<ClienteRutinaDTO> getDataClienteRutina()
        throws Exception {
        return clienteRutinaService.getDataClienteRutina();
    }

    public void validateClienteRutina(ClienteRutina clienteRutina)
        throws Exception {
        clienteRutinaService.validateClienteRutina(clienteRutina);
    }

    public List<Ejercicio> getEjercicio() throws Exception {
        return ejercicioService.getEjercicio();
    }

    public void saveEjercicio(Ejercicio entity) throws Exception {
        ejercicioService.saveEjercicio(entity);
    }

    public void deleteEjercicio(Ejercicio entity) throws Exception {
        ejercicioService.deleteEjercicio(entity);
    }

    public void updateEjercicio(Ejercicio entity) throws Exception {
        ejercicioService.updateEjercicio(entity);
    }

    public Ejercicio getEjercicio(Long ejeId) throws Exception {
        Ejercicio ejercicio = null;

        try {
            ejercicio = ejercicioService.getEjercicio(ejeId);
        } catch (Exception e) {
            throw e;
        }

        return ejercicio;
    }

    public List<Ejercicio> findByCriteriaInEjercicio(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return ejercicioService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Ejercicio> findPageEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return ejercicioService.findPageEjercicio(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberEjercicio() throws Exception {
        return ejercicioService.findTotalNumberEjercicio();
    }

    public List<EjercicioDTO> getDataEjercicio() throws Exception {
        return ejercicioService.getDataEjercicio();
    }

    public void validateEjercicio(Ejercicio ejercicio)
        throws Exception {
        ejercicioService.validateEjercicio(ejercicio);
    }

    public List<EjercicioRutina> getEjercicioRutina() throws Exception {
        return ejercicioRutinaService.getEjercicioRutina();
    }

    public void saveEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaService.saveEjercicioRutina(entity);
    }

    public void deleteEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaService.deleteEjercicioRutina(entity);
    }

    public void updateEjercicioRutina(EjercicioRutina entity)
        throws Exception {
        ejercicioRutinaService.updateEjercicioRutina(entity);
    }

    public EjercicioRutina getEjercicioRutina(Long ejerutId)
        throws Exception {
        EjercicioRutina ejercicioRutina = null;

        try {
            ejercicioRutina = ejercicioRutinaService.getEjercicioRutina(ejerutId);
        } catch (Exception e) {
            throw e;
        }

        return ejercicioRutina;
    }

    public List<EjercicioRutina> findByCriteriaInEjercicioRutina(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return ejercicioRutinaService.findByCriteria(variables,
            variablesBetween, variablesBetweenDates);
    }

    public List<EjercicioRutina> findPageEjercicioRutina(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return ejercicioRutinaService.findPageEjercicioRutina(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberEjercicioRutina() throws Exception {
        return ejercicioRutinaService.findTotalNumberEjercicioRutina();
    }

    public List<EjercicioRutinaDTO> getDataEjercicioRutina()
        throws Exception {
        return ejercicioRutinaService.getDataEjercicioRutina();
    }

    public void validateEjercicioRutina(EjercicioRutina ejercicioRutina)
        throws Exception {
        ejercicioRutinaService.validateEjercicioRutina(ejercicioRutina);
    }

    public List<Empleado> getEmpleado() throws Exception {
        return empleadoService.getEmpleado();
    }

    public void saveEmpleado(Empleado entity) throws Exception {
        empleadoService.saveEmpleado(entity);
    }

    public void deleteEmpleado(Empleado entity) throws Exception {
        empleadoService.deleteEmpleado(entity);
    }

    public void updateEmpleado(Empleado entity) throws Exception {
        empleadoService.updateEmpleado(entity);
    }

    public Empleado getEmpleado(Long empId) throws Exception {
        Empleado empleado = null;

        try {
            empleado = empleadoService.getEmpleado(empId);
        } catch (Exception e) {
            throw e;
        }

        return empleado;
    }

    public List<Empleado> findByCriteriaInEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return empleadoService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Empleado> findPageEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return empleadoService.findPageEmpleado(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberEmpleado() throws Exception {
        return empleadoService.findTotalNumberEmpleado();
    }

    public List<EmpleadoDTO> getDataEmpleado() throws Exception {
        return empleadoService.getDataEmpleado();
    }

    public void validateEmpleado(Empleado empleado) throws Exception {
        empleadoService.validateEmpleado(empleado);
    }

    public List<Pago> getPago() throws Exception {
        return pagoService.getPago();
    }

    public void savePago(Pago entity) throws Exception {
        pagoService.savePago(entity);
    }

    public void deletePago(Pago entity) throws Exception {
        pagoService.deletePago(entity);
    }

    public void updatePago(Pago entity) throws Exception {
        pagoService.updatePago(entity);
    }

    public Pago getPago(Long pagId) throws Exception {
        Pago pago = null;

        try {
            pago = pagoService.getPago(pagId);
        } catch (Exception e) {
            throw e;
        }

        return pago;
    }

    public List<Pago> findByCriteriaInPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return pagoService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Pago> findPagePago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return pagoService.findPagePago(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberPago() throws Exception {
        return pagoService.findTotalNumberPago();
    }

    public List<PagoDTO> getDataPago() throws Exception {
        return pagoService.getDataPago();
    }

    public void validatePago(Pago pago) throws Exception {
        pagoService.validatePago(pago);
    }

    public List<Plan> getPlan() throws Exception {
        return planService.getPlan();
    }

    public void savePlan(Plan entity) throws Exception {
        planService.savePlan(entity);
    }

    public void deletePlan(Plan entity) throws Exception {
        planService.deletePlan(entity);
    }

    public void updatePlan(Plan entity) throws Exception {
        planService.updatePlan(entity);
    }

    public Plan getPlan(Long plaId) throws Exception {
        Plan plan = null;

        try {
            plan = planService.getPlan(plaId);
        } catch (Exception e) {
            throw e;
        }

        return plan;
    }

    public List<Plan> findByCriteriaInPlan(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return planService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Plan> findPagePlan(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return planService.findPagePlan(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberPlan() throws Exception {
        return planService.findTotalNumberPlan();
    }

    public List<PlanDTO> getDataPlan() throws Exception {
        return planService.getDataPlan();
    }

    public void validatePlan(Plan plan) throws Exception {
        planService.validatePlan(plan);
    }

    public List<PlanCliente> getPlanCliente() throws Exception {
        return planClienteService.getPlanCliente();
    }

    public void savePlanCliente(PlanCliente entity) throws Exception {
        planClienteService.savePlanCliente(entity);
    }

    public void deletePlanCliente(PlanCliente entity) throws Exception {
        planClienteService.deletePlanCliente(entity);
    }

    public void updatePlanCliente(PlanCliente entity) throws Exception {
        planClienteService.updatePlanCliente(entity);
    }

    public PlanCliente getPlanCliente(Long plancliId) throws Exception {
        PlanCliente planCliente = null;

        try {
            planCliente = planClienteService.getPlanCliente(plancliId);
        } catch (Exception e) {
            throw e;
        }

        return planCliente;
    }

    public List<PlanCliente> findByCriteriaInPlanCliente(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return planClienteService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<PlanCliente> findPagePlanCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return planClienteService.findPagePlanCliente(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberPlanCliente() throws Exception {
        return planClienteService.findTotalNumberPlanCliente();
    }

    public List<PlanClienteDTO> getDataPlanCliente() throws Exception {
        return planClienteService.getDataPlanCliente();
    }

    public void validatePlanCliente(PlanCliente planCliente)
        throws Exception {
        planClienteService.validatePlanCliente(planCliente);
    }

    public List<Rutina> getRutina() throws Exception {
        return rutinaService.getRutina();
    }

    public void saveRutina(Rutina entity) throws Exception {
        rutinaService.saveRutina(entity);
    }

    public void deleteRutina(Rutina entity) throws Exception {
        rutinaService.deleteRutina(entity);
    }

    public void updateRutina(Rutina entity) throws Exception {
        rutinaService.updateRutina(entity);
    }

    public Rutina getRutina(String rutId) throws Exception {
        Rutina rutina = null;

        try {
            rutina = rutinaService.getRutina(rutId);
        } catch (Exception e) {
            throw e;
        }

        return rutina;
    }

    public List<Rutina> findByCriteriaInRutina(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return rutinaService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Rutina> findPageRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return rutinaService.findPageRutina(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberRutina() throws Exception {
        return rutinaService.findTotalNumberRutina();
    }

    public List<RutinaDTO> getDataRutina() throws Exception {
        return rutinaService.getDataRutina();
    }

    public void validateRutina(Rutina rutina) throws Exception {
        rutinaService.validateRutina(rutina);
    }

    public List<TipoEjercicio> getTipoEjercicio() throws Exception {
        return tipoEjercicioService.getTipoEjercicio();
    }

    public void saveTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioService.saveTipoEjercicio(entity);
    }

    public void deleteTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioService.deleteTipoEjercicio(entity);
    }

    public void updateTipoEjercicio(TipoEjercicio entity)
        throws Exception {
        tipoEjercicioService.updateTipoEjercicio(entity);
    }

    public TipoEjercicio getTipoEjercicio(Long tiejeId)
        throws Exception {
        TipoEjercicio tipoEjercicio = null;

        try {
            tipoEjercicio = tipoEjercicioService.getTipoEjercicio(tiejeId);
        } catch (Exception e) {
            throw e;
        }

        return tipoEjercicio;
    }

    public List<TipoEjercicio> findByCriteriaInTipoEjercicio(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return tipoEjercicioService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoEjercicio> findPageTipoEjercicio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoEjercicioService.findPageTipoEjercicio(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberTipoEjercicio() throws Exception {
        return tipoEjercicioService.findTotalNumberTipoEjercicio();
    }

    public List<TipoEjercicioDTO> getDataTipoEjercicio()
        throws Exception {
        return tipoEjercicioService.getDataTipoEjercicio();
    }

    public void validateTipoEjercicio(TipoEjercicio tipoEjercicio)
        throws Exception {
        tipoEjercicioService.validateTipoEjercicio(tipoEjercicio);
    }

    public List<TipoEmpleado> getTipoEmpleado() throws Exception {
        return tipoEmpleadoService.getTipoEmpleado();
    }

    public void saveTipoEmpleado(TipoEmpleado entity) throws Exception {
        tipoEmpleadoService.saveTipoEmpleado(entity);
    }

    public void deleteTipoEmpleado(TipoEmpleado entity)
        throws Exception {
        tipoEmpleadoService.deleteTipoEmpleado(entity);
    }

    public void updateTipoEmpleado(TipoEmpleado entity)
        throws Exception {
        tipoEmpleadoService.updateTipoEmpleado(entity);
    }

    public TipoEmpleado getTipoEmpleado(Long tiemId) throws Exception {
        TipoEmpleado tipoEmpleado = null;

        try {
            tipoEmpleado = tipoEmpleadoService.getTipoEmpleado(tiemId);
        } catch (Exception e) {
            throw e;
        }

        return tipoEmpleado;
    }

    public List<TipoEmpleado> findByCriteriaInTipoEmpleado(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return tipoEmpleadoService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoEmpleado> findPageTipoEmpleado(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoEmpleadoService.findPageTipoEmpleado(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberTipoEmpleado() throws Exception {
        return tipoEmpleadoService.findTotalNumberTipoEmpleado();
    }

    public List<TipoEmpleadoDTO> getDataTipoEmpleado()
        throws Exception {
        return tipoEmpleadoService.getDataTipoEmpleado();
    }

    public void validateTipoEmpleado(TipoEmpleado tipoEmpleado)
        throws Exception {
        tipoEmpleadoService.validateTipoEmpleado(tipoEmpleado);
    }

    public List<TipoPago> getTipoPago() throws Exception {
        return tipoPagoService.getTipoPago();
    }

    public void saveTipoPago(TipoPago entity) throws Exception {
        tipoPagoService.saveTipoPago(entity);
    }

    public void deleteTipoPago(TipoPago entity) throws Exception {
        tipoPagoService.deleteTipoPago(entity);
    }

    public void updateTipoPago(TipoPago entity) throws Exception {
        tipoPagoService.updateTipoPago(entity);
    }

    public TipoPago getTipoPago(Long tipaId) throws Exception {
        TipoPago tipoPago = null;

        try {
            tipoPago = tipoPagoService.getTipoPago(tipaId);
        } catch (Exception e) {
            throw e;
        }

        return tipoPago;
    }

    public List<TipoPago> findByCriteriaInTipoPago(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return tipoPagoService.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoPago> findPageTipoPago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoPagoService.findPageTipoPago(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberTipoPago() throws Exception {
        return tipoPagoService.findTotalNumberTipoPago();
    }

    public List<TipoPagoDTO> getDataTipoPago() throws Exception {
        return tipoPagoService.getDataTipoPago();
    }

    public void validateTipoPago(TipoPago tipoPago) throws Exception {
        tipoPagoService.validateTipoPago(tipoPago);
    }
}
