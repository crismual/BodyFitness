package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface EjercicioRutinaService {
    public List<EjercicioRutina> getEjercicioRutina() throws Exception;

    /**
         * Save an new EjercicioRutina entity
         */
    public void saveEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    /**
         * Delete an existing EjercicioRutina entity
         *
         */
    public void deleteEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    /**
        * Update an existing EjercicioRutina entity
        *
        */
    public void updateEjercicioRutina(EjercicioRutina entity)
        throws Exception;

    /**
         * Load an existing EjercicioRutina entity
         *
         */
    public EjercicioRutina getEjercicioRutina(Long ejerutId)
        throws Exception;

    public List<EjercicioRutina> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<EjercicioRutina> findPageEjercicioRutina(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberEjercicioRutina() throws Exception;

    public List<EjercicioRutinaDTO> getDataEjercicioRutina()
        throws Exception;

    public void validateEjercicioRutina(EjercicioRutina ejercicioRutina)
        throws Exception;
}
