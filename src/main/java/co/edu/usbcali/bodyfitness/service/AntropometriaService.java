package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.AntropometriaDTO;
import co.edu.usbcali.bodyfitness.modelo.Antropometria;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface AntropometriaService {
    public List<Antropometria> getAntropometria() throws Exception;

    /**
         * Save an new Antropometria entity
         */
    public void saveAntropometria(Antropometria entity)
        throws Exception;

    /**
         * Delete an existing Antropometria entity
         *
         */
    public void deleteAntropometria(Antropometria entity)
        throws Exception;

    /**
        * Update an existing Antropometria entity
        *
        */
    public void updateAntropometria(Antropometria entity)
        throws Exception;

    /**
         * Load an existing Antropometria entity
         *
         */
    public Antropometria getAntropometria(Long antId) throws Exception;

    public List<Antropometria> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Antropometria> findPageAntropometria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberAntropometria() throws Exception;

    public List<AntropometriaDTO> getDataAntropometria()
        throws Exception;

    public void validateAntropometria(Antropometria antropometria)
        throws Exception;
}
