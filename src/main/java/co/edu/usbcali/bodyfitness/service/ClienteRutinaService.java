package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.ClienteRutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ClienteRutinaService {
    public List<ClienteRutina> getClienteRutina() throws Exception;

    /**
         * Save an new ClienteRutina entity
         */
    public void saveClienteRutina(ClienteRutina entity)
        throws Exception;

    /**
         * Delete an existing ClienteRutina entity
         *
         */
    public void deleteClienteRutina(ClienteRutina entity)
        throws Exception;

    /**
        * Update an existing ClienteRutina entity
        *
        */
    public void updateClienteRutina(ClienteRutina entity)
        throws Exception;

    /**
         * Load an existing ClienteRutina entity
         *
         */
    public ClienteRutina getClienteRutina(Long clirutId)
        throws Exception;

    public List<ClienteRutina> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<ClienteRutina> findPageClienteRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberClienteRutina() throws Exception;

    public List<ClienteRutinaDTO> getDataClienteRutina()
        throws Exception;

    public void validateClienteRutina(ClienteRutina clienteRutina)
        throws Exception;
}
