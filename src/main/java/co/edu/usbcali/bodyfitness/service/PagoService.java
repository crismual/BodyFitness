package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.PagoDTO;
import co.edu.usbcali.bodyfitness.modelo.Pago;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface PagoService {
    public List<Pago> getPago() throws Exception;

    /**
         * Save an new Pago entity
         */
    public void savePago(Pago entity) throws Exception;

    /**
         * Delete an existing Pago entity
         *
         */
    public void deletePago(Pago entity) throws Exception;

    /**
        * Update an existing Pago entity
        *
        */
    public void updatePago(Pago entity) throws Exception;

    /**
         * Load an existing Pago entity
         *
         */
    public Pago getPago(Long pagId) throws Exception;

    public List<Pago> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Pago> findPagePago(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPago() throws Exception;

    public List<PagoDTO> getDataPago() throws Exception;

    public void validatePago(Pago pago) throws Exception;
}
