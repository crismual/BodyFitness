package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.PlanClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.PlanCliente;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface PlanClienteService {
    public List<PlanCliente> getPlanCliente() throws Exception;

    /**
         * Save an new PlanCliente entity
         */
    public void savePlanCliente(PlanCliente entity) throws Exception;

    /**
         * Delete an existing PlanCliente entity
         *
         */
    public void deletePlanCliente(PlanCliente entity) throws Exception;

    /**
        * Update an existing PlanCliente entity
        *
        */
    public void updatePlanCliente(PlanCliente entity) throws Exception;

    /**
         * Load an existing PlanCliente entity
         *
         */
    public PlanCliente getPlanCliente(Long plancliId) throws Exception;

    public List<PlanCliente> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<PlanCliente> findPagePlanCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPlanCliente() throws Exception;

    public List<PlanClienteDTO> getDataPlanCliente() throws Exception;

    public void validatePlanCliente(PlanCliente planCliente)
        throws Exception;
}
