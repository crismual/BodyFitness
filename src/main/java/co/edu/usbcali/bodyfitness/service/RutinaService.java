package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.modelo.Rutina;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface RutinaService {
    public List<Rutina> getRutina() throws Exception;

    /**
         * Save an new Rutina entity
         */
    public void saveRutina(Rutina entity) throws Exception;

    /**
         * Delete an existing Rutina entity
         *
         */
    public void deleteRutina(Rutina entity) throws Exception;

    /**
        * Update an existing Rutina entity
        *
        */
    public void updateRutina(Rutina entity) throws Exception;

    /**
         * Load an existing Rutina entity
         *
         */
    public Rutina getRutina(String rutId) throws Exception;

    public List<Rutina> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Rutina> findPageRutina(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberRutina() throws Exception;

    public List<RutinaDTO> getDataRutina() throws Exception;

    public void validateRutina(Rutina rutina) throws Exception;
}
