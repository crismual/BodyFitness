package co.edu.usbcali.bodyfitness.service;

import co.edu.usbcali.bodyfitness.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.modelo.Cliente;

import java.math.*;

import java.util.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface ClienteService {
    public List<Cliente> getCliente() throws Exception;

    /**
         * Save an new Cliente entity
         */
    public void saveCliente(Cliente entity) throws Exception;

    /**
         * Delete an existing Cliente entity
         *
         */
    public void deleteCliente(Cliente entity) throws Exception;

    /**
        * Update an existing Cliente entity
        *
        */
    public void updateCliente(Cliente entity) throws Exception;

    /**
         * Load an existing Cliente entity
         *
         */
    public Cliente getCliente(Long cliId) throws Exception;

    public List<Cliente> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Cliente> findPageCliente(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberCliente() throws Exception;

    public List<ClienteDTO> getDataCliente() throws Exception;

    public void validateCliente(Cliente cliente) throws Exception;
}
