package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.TipoEjercicioDTO;
import co.edu.usbcali.bodyfitness.mapper.TipoEjercicioMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.TipoEjercicioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/tipoEjercicio")
public class TipoEjercicioRestController {
    private static final Logger log = LoggerFactory.getLogger(TipoEjercicioRestController.class);
    @Autowired
    private TipoEjercicioService tipoEjercicioService;
    @Autowired
    private TipoEjercicioMapper tipoEjercicioMapper;

    @PostMapping(value = "/saveTipoEjercicio")
    public void saveTipoEjercicio(@RequestBody
    TipoEjercicioDTO tipoEjercicioDTO) throws Exception {
        try {
            TipoEjercicio tipoEjercicio = tipoEjercicioMapper.tipoEjercicioDTOToTipoEjercicio(tipoEjercicioDTO);

            tipoEjercicioService.saveTipoEjercicio(tipoEjercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteTipoEjercicio/{tiejeId}")
    public void deleteTipoEjercicio(@PathVariable("tiejeId")
    Long tiejeId) throws Exception {
        try {
            TipoEjercicio tipoEjercicio = tipoEjercicioService.getTipoEjercicio(tiejeId);

            tipoEjercicioService.deleteTipoEjercicio(tipoEjercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateTipoEjercicio/")
    public void updateTipoEjercicio(
        @RequestBody
    TipoEjercicioDTO tipoEjercicioDTO) throws Exception {
        try {
            TipoEjercicio tipoEjercicio = tipoEjercicioMapper.tipoEjercicioDTOToTipoEjercicio(tipoEjercicioDTO);

            tipoEjercicioService.updateTipoEjercicio(tipoEjercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataTipoEjercicio")
    public List<TipoEjercicioDTO> getDataTipoEjercicio()
        throws Exception {
        try {
            return tipoEjercicioService.getDataTipoEjercicio();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getTipoEjercicio/{tiejeId}")
    public TipoEjercicioDTO getTipoEjercicio(
        @PathVariable("tiejeId")
    Long tiejeId) throws Exception {
        try {
            TipoEjercicio tipoEjercicio = tipoEjercicioService.getTipoEjercicio(tiejeId);

            return tipoEjercicioMapper.tipoEjercicioToTipoEjercicioDTO(tipoEjercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
