package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.RutinaDTO;
import co.edu.usbcali.bodyfitness.mapper.RutinaMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.RutinaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/rutina")
public class RutinaRestController {
    private static final Logger log = LoggerFactory.getLogger(RutinaRestController.class);
    @Autowired
    private RutinaService rutinaService;
    @Autowired
    private RutinaMapper rutinaMapper;

    @PostMapping(value = "/saveRutina")
    public void saveRutina(@RequestBody
    RutinaDTO rutinaDTO) throws Exception {
        try {
            Rutina rutina = rutinaMapper.rutinaDTOToRutina(rutinaDTO);

            rutinaService.saveRutina(rutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteRutina/{rutId}")
    public void deleteRutina(@PathVariable("rutId")
    String rutId) throws Exception {
        try {
            Rutina rutina = rutinaService.getRutina(rutId);

            rutinaService.deleteRutina(rutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateRutina/")
    public void updateRutina(@RequestBody
    RutinaDTO rutinaDTO) throws Exception {
        try {
            Rutina rutina = rutinaMapper.rutinaDTOToRutina(rutinaDTO);

            rutinaService.updateRutina(rutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataRutina")
    public List<RutinaDTO> getDataRutina() throws Exception {
        try {
            return rutinaService.getDataRutina();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getRutina/{rutId}")
    public RutinaDTO getRutina(@PathVariable("rutId")
    String rutId) throws Exception {
        try {
            Rutina rutina = rutinaService.getRutina(rutId);

            return rutinaMapper.rutinaToRutinaDTO(rutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
