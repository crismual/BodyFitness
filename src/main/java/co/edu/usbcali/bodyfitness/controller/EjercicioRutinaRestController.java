package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.EjercicioRutinaDTO;
import co.edu.usbcali.bodyfitness.mapper.EjercicioRutinaMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.EjercicioRutinaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/ejercicioRutina")
public class EjercicioRutinaRestController {
    private static final Logger log = LoggerFactory.getLogger(EjercicioRutinaRestController.class);
    @Autowired
    private EjercicioRutinaService ejercicioRutinaService;
    @Autowired
    private EjercicioRutinaMapper ejercicioRutinaMapper;

    @PostMapping(value = "/saveEjercicioRutina")
    public void saveEjercicioRutina(
        @RequestBody
    EjercicioRutinaDTO ejercicioRutinaDTO) throws Exception {
        try {
            EjercicioRutina ejercicioRutina = ejercicioRutinaMapper.ejercicioRutinaDTOToEjercicioRutina(ejercicioRutinaDTO);

            ejercicioRutinaService.saveEjercicioRutina(ejercicioRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteEjercicioRutina/{ejerutId}")
    public void deleteEjercicioRutina(@PathVariable("ejerutId")
    Long ejerutId) throws Exception {
        try {
            EjercicioRutina ejercicioRutina = ejercicioRutinaService.getEjercicioRutina(ejerutId);

            ejercicioRutinaService.deleteEjercicioRutina(ejercicioRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateEjercicioRutina/")
    public void updateEjercicioRutina(
        @RequestBody
    EjercicioRutinaDTO ejercicioRutinaDTO) throws Exception {
        try {
            EjercicioRutina ejercicioRutina = ejercicioRutinaMapper.ejercicioRutinaDTOToEjercicioRutina(ejercicioRutinaDTO);

            ejercicioRutinaService.updateEjercicioRutina(ejercicioRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataEjercicioRutina")
    public List<EjercicioRutinaDTO> getDataEjercicioRutina()
        throws Exception {
        try {
            return ejercicioRutinaService.getDataEjercicioRutina();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getEjercicioRutina/{ejerutId}")
    public EjercicioRutinaDTO getEjercicioRutina(
        @PathVariable("ejerutId")
    Long ejerutId) throws Exception {
        try {
            EjercicioRutina ejercicioRutina = ejercicioRutinaService.getEjercicioRutina(ejerutId);

            return ejercicioRutinaMapper.ejercicioRutinaToEjercicioRutinaDTO(ejercicioRutina);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
