package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.TipoPagoDTO;
import co.edu.usbcali.bodyfitness.mapper.TipoPagoMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.TipoPagoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/tipoPago")
public class TipoPagoRestController {
    private static final Logger log = LoggerFactory.getLogger(TipoPagoRestController.class);
    @Autowired
    private TipoPagoService tipoPagoService;
    @Autowired
    private TipoPagoMapper tipoPagoMapper;

    @PostMapping(value = "/saveTipoPago")
    public void saveTipoPago(@RequestBody
    TipoPagoDTO tipoPagoDTO) throws Exception {
        try {
            TipoPago tipoPago = tipoPagoMapper.tipoPagoDTOToTipoPago(tipoPagoDTO);

            tipoPagoService.saveTipoPago(tipoPago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteTipoPago/{tipaId}")
    public void deleteTipoPago(@PathVariable("tipaId")
    Long tipaId) throws Exception {
        try {
            TipoPago tipoPago = tipoPagoService.getTipoPago(tipaId);

            tipoPagoService.deleteTipoPago(tipoPago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateTipoPago/")
    public void updateTipoPago(@RequestBody
    TipoPagoDTO tipoPagoDTO) throws Exception {
        try {
            TipoPago tipoPago = tipoPagoMapper.tipoPagoDTOToTipoPago(tipoPagoDTO);

            tipoPagoService.updateTipoPago(tipoPago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataTipoPago")
    public List<TipoPagoDTO> getDataTipoPago() throws Exception {
        try {
            return tipoPagoService.getDataTipoPago();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getTipoPago/{tipaId}")
    public TipoPagoDTO getTipoPago(@PathVariable("tipaId")
    Long tipaId) throws Exception {
        try {
            TipoPago tipoPago = tipoPagoService.getTipoPago(tipaId);

            return tipoPagoMapper.tipoPagoToTipoPagoDTO(tipoPago);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
