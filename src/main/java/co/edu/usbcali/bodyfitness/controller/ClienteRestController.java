package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.ClienteDTO;
import co.edu.usbcali.bodyfitness.mapper.ClienteMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.ClienteService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/cliente")
public class ClienteRestController {
    private static final Logger log = LoggerFactory.getLogger(ClienteRestController.class);
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping(value = "/saveCliente")
    public void saveCliente(@RequestBody
    ClienteDTO clienteDTO) throws Exception {
        try {
            Cliente cliente = clienteMapper.clienteDTOToCliente(clienteDTO);

            clienteService.saveCliente(cliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteCliente/{cliId}")
    public void deleteCliente(@PathVariable("cliId")
    Long cliId) throws Exception {
        try {
            Cliente cliente = clienteService.getCliente(cliId);

            clienteService.deleteCliente(cliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateCliente/")
    public void updateCliente(@RequestBody
    ClienteDTO clienteDTO) throws Exception {
        try {
            Cliente cliente = clienteMapper.clienteDTOToCliente(clienteDTO);

            clienteService.updateCliente(cliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataCliente")
    public List<ClienteDTO> getDataCliente() throws Exception {
        try {
            return clienteService.getDataCliente();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getCliente/{cliId}")
    public ClienteDTO getCliente(@PathVariable("cliId")
    Long cliId) throws Exception {
        try {
            Cliente cliente = clienteService.getCliente(cliId);

            return clienteMapper.clienteToClienteDTO(cliente);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
