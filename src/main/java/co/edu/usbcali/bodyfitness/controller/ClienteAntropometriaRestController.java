package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.ClienteAntropometriaDTO;
import co.edu.usbcali.bodyfitness.mapper.ClienteAntropometriaMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.ClienteAntropometriaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/clienteAntropometria")
public class ClienteAntropometriaRestController {
    private static final Logger log = LoggerFactory.getLogger(ClienteAntropometriaRestController.class);
    @Autowired
    private ClienteAntropometriaService clienteAntropometriaService;
    @Autowired
    private ClienteAntropometriaMapper clienteAntropometriaMapper;

    @PostMapping(value = "/saveClienteAntropometria")
    public void saveClienteAntropometria(
        @RequestBody
    ClienteAntropometriaDTO clienteAntropometriaDTO) throws Exception {
        try {
            ClienteAntropometria clienteAntropometria = clienteAntropometriaMapper.clienteAntropometriaDTOToClienteAntropometria(clienteAntropometriaDTO);

            clienteAntropometriaService.saveClienteAntropometria(clienteAntropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteClienteAntropometria/{cliantId}")
    public void deleteClienteAntropometria(
        @PathVariable("cliantId")
    Long cliantId) throws Exception {
        try {
            ClienteAntropometria clienteAntropometria = clienteAntropometriaService.getClienteAntropometria(cliantId);

            clienteAntropometriaService.deleteClienteAntropometria(clienteAntropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateClienteAntropometria/")
    public void updateClienteAntropometria(
        @RequestBody
    ClienteAntropometriaDTO clienteAntropometriaDTO) throws Exception {
        try {
            ClienteAntropometria clienteAntropometria = clienteAntropometriaMapper.clienteAntropometriaDTOToClienteAntropometria(clienteAntropometriaDTO);

            clienteAntropometriaService.updateClienteAntropometria(clienteAntropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataClienteAntropometria")
    public List<ClienteAntropometriaDTO> getDataClienteAntropometria()
        throws Exception {
        try {
            return clienteAntropometriaService.getDataClienteAntropometria();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getClienteAntropometria/{cliantId}")
    public ClienteAntropometriaDTO getClienteAntropometria(
        @PathVariable("cliantId")
    Long cliantId) throws Exception {
        try {
            ClienteAntropometria clienteAntropometria = clienteAntropometriaService.getClienteAntropometria(cliantId);

            return clienteAntropometriaMapper.clienteAntropometriaToClienteAntropometriaDTO(clienteAntropometria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
