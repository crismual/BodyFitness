package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.TipoEmpleadoDTO;
import co.edu.usbcali.bodyfitness.mapper.TipoEmpleadoMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.TipoEmpleadoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/tipoEmpleado")
public class TipoEmpleadoRestController {
    private static final Logger log = LoggerFactory.getLogger(TipoEmpleadoRestController.class);
    @Autowired
    private TipoEmpleadoService tipoEmpleadoService;
    @Autowired
    private TipoEmpleadoMapper tipoEmpleadoMapper;

    @PostMapping(value = "/saveTipoEmpleado")
    public void saveTipoEmpleado(@RequestBody
    TipoEmpleadoDTO tipoEmpleadoDTO) throws Exception {
        try {
            TipoEmpleado tipoEmpleado = tipoEmpleadoMapper.tipoEmpleadoDTOToTipoEmpleado(tipoEmpleadoDTO);

            tipoEmpleadoService.saveTipoEmpleado(tipoEmpleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteTipoEmpleado/{tiemId}")
    public void deleteTipoEmpleado(@PathVariable("tiemId")
    Long tiemId) throws Exception {
        try {
            TipoEmpleado tipoEmpleado = tipoEmpleadoService.getTipoEmpleado(tiemId);

            tipoEmpleadoService.deleteTipoEmpleado(tipoEmpleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateTipoEmpleado/")
    public void updateTipoEmpleado(@RequestBody
    TipoEmpleadoDTO tipoEmpleadoDTO) throws Exception {
        try {
            TipoEmpleado tipoEmpleado = tipoEmpleadoMapper.tipoEmpleadoDTOToTipoEmpleado(tipoEmpleadoDTO);

            tipoEmpleadoService.updateTipoEmpleado(tipoEmpleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataTipoEmpleado")
    public List<TipoEmpleadoDTO> getDataTipoEmpleado()
        throws Exception {
        try {
            return tipoEmpleadoService.getDataTipoEmpleado();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getTipoEmpleado/{tiemId}")
    public TipoEmpleadoDTO getTipoEmpleado(@PathVariable("tiemId")
    Long tiemId) throws Exception {
        try {
            TipoEmpleado tipoEmpleado = tipoEmpleadoService.getTipoEmpleado(tiemId);

            return tipoEmpleadoMapper.tipoEmpleadoToTipoEmpleadoDTO(tipoEmpleado);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
