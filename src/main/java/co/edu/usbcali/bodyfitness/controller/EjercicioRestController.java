package co.edu.usbcali.bodyfitness.controller;

import co.edu.usbcali.bodyfitness.dto.EjercicioDTO;
import co.edu.usbcali.bodyfitness.mapper.EjercicioMapper;
import co.edu.usbcali.bodyfitness.modelo.*;
import co.edu.usbcali.bodyfitness.service.EjercicioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/ejercicio")
public class EjercicioRestController {
    private static final Logger log = LoggerFactory.getLogger(EjercicioRestController.class);
    @Autowired
    private EjercicioService ejercicioService;
    @Autowired
    private EjercicioMapper ejercicioMapper;

    @PostMapping(value = "/saveEjercicio")
    public void saveEjercicio(@RequestBody
    EjercicioDTO ejercicioDTO) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioMapper.ejercicioDTOToEjercicio(ejercicioDTO);

            ejercicioService.saveEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteEjercicio/{ejeId}")
    public void deleteEjercicio(@PathVariable("ejeId")
    Long ejeId) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioService.getEjercicio(ejeId);

            ejercicioService.deleteEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateEjercicio/")
    public void updateEjercicio(@RequestBody
    EjercicioDTO ejercicioDTO) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioMapper.ejercicioDTOToEjercicio(ejercicioDTO);

            ejercicioService.updateEjercicio(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataEjercicio")
    public List<EjercicioDTO> getDataEjercicio() throws Exception {
        try {
            return ejercicioService.getDataEjercicio();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getEjercicio/{ejeId}")
    public EjercicioDTO getEjercicio(@PathVariable("ejeId")
    Long ejeId) throws Exception {
        try {
            Ejercicio ejercicio = ejercicioService.getEjercicio(ejeId);

            return ejercicioMapper.ejercicioToEjercicioDTO(ejercicio);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}
