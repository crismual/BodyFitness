package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Pago;

import java.math.BigDecimal;


/**
* Interface for   PagoRepository.
*
*/
public interface PagoRepository extends JpaGenericRepository<Pago, Long> {
}
