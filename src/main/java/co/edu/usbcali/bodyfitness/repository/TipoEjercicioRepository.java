package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.TipoEjercicio;

import java.math.BigDecimal;


/**
* Interface for   TipoEjercicioRepository.
*
*/
public interface TipoEjercicioRepository extends JpaGenericRepository<TipoEjercicio, Long> {
}
