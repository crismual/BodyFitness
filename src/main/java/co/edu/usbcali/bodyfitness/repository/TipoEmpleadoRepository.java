package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.TipoEmpleado;

import java.math.BigDecimal;


/**
* Interface for   TipoEmpleadoRepository.
*
*/
public interface TipoEmpleadoRepository extends JpaGenericRepository<TipoEmpleado, Long> {
}
