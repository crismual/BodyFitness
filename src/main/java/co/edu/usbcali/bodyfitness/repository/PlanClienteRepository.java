package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.PlanCliente;

import java.math.BigDecimal;


/**
* Interface for   PlanClienteRepository.
*
*/
public interface PlanClienteRepository extends JpaGenericRepository<PlanCliente, Long> {
}
