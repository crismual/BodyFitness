package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.EjercicioRutina;

import java.math.BigDecimal;


/**
* Interface for   EjercicioRutinaRepository.
*
*/
public interface EjercicioRutinaRepository extends JpaGenericRepository<EjercicioRutina, Long> {
}
