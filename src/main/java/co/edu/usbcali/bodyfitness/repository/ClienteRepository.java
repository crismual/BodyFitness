package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Cliente;

import java.math.BigDecimal;


/**
* Interface for   ClienteRepository.
*
*/
public interface ClienteRepository extends JpaGenericRepository<Cliente, Long> {
}
