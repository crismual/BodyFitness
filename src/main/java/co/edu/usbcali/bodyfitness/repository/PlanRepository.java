package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Plan;

import java.math.BigDecimal;


/**
* Interface for   PlanRepository.
*
*/
public interface PlanRepository extends JpaGenericRepository<Plan, Long> {
}
