package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Rutina;

import java.math.BigDecimal;


/**
* Interface for   RutinaRepository.
*
*/
public interface RutinaRepository extends JpaGenericRepository<Rutina, String> {
}
