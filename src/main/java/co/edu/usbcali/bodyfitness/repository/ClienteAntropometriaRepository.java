package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.ClienteAntropometria;

import java.math.BigDecimal;


/**
* Interface for   ClienteAntropometriaRepository.
*
*/
public interface ClienteAntropometriaRepository extends JpaGenericRepository<ClienteAntropometria, Long> {
}
