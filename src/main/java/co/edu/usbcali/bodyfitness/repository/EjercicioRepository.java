package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Ejercicio;

import java.math.BigDecimal;


/**
* Interface for   EjercicioRepository.
*
*/
public interface EjercicioRepository extends JpaGenericRepository<Ejercicio, Long> {
}
