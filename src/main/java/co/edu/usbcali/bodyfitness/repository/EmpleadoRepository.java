package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Empleado;

import java.math.BigDecimal;


/**
* Interface for   EmpleadoRepository.
*
*/
public interface EmpleadoRepository extends JpaGenericRepository<Empleado, Long> {
}
