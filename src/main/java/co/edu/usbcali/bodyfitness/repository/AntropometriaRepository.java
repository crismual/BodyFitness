package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.Antropometria;

import java.math.BigDecimal;


/**
* Interface for   AntropometriaRepository.
*
*/
public interface AntropometriaRepository extends JpaGenericRepository<Antropometria, Long> {
}
