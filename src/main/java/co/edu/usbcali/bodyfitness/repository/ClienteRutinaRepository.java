package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.ClienteRutina;

import java.math.BigDecimal;


/**
* Interface for   ClienteRutinaRepository.
*
*/
public interface ClienteRutinaRepository extends JpaGenericRepository<ClienteRutina, Long> {
}
