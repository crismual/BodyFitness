package co.edu.usbcali.bodyfitness.repository;

import co.edu.usbcali.bodyfitness.modelo.TipoPago;

import java.math.BigDecimal;


/**
* Interface for   TipoPagoRepository.
*
*/
public interface TipoPagoRepository extends JpaGenericRepository<TipoPago, Long> {
}
